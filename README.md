# Getting Started

`$ npm install -g audit-html`

# Dependency

kindly check for npm installation

`npm -v`

if not found then install stable node package

from node official website: https://nodejs.org/en/

Once installation is completed, kindly follow the usage steps.

# Usage

`npm audit --json | audit-html`

By default the report will be saved to audit.html

If you want to specify the output file, add the --output option:

`npm audit --json | audit-html --output customname.html`
